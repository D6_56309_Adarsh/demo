const mysql=require('mysql')

function openConnection(){
    const connection=mysql.createConnection({
        host:'db',
        user:'root',
        password:'root',
        port:'3306',
        database:'mydb'
    })
    connection.connect()

    return connection
}
module.exports={
    openConnection
}